var title = document.getElementById("title");
var description = document.getElementById("description");
var file= document.getElementById("image");


title.addEventListener("keyup", function(ev){
        ev.preventDefault();
        var regex =  /^[a-zA-Z].{5,100}/;
        var titleCheck = title.value;
        var titleContainer = document.getElementById("title-container");
        if ( !titleCheck.match(regex)) {
            titleContainer.classList.add("has-error");
            }
        else{
            titleContainer.classList.remove("has-error");
            titleContainer.classList.add("has-success");

        }

    })

description.addEventListener("keyup", function(ev){
    ev.preventDefault();
    var regex = /^[a-zA-Z].{5,100}/;
    var descriptCheck = description.value;
    var descriptionContainer = document.getElementById("descriptionContainer");
    
    if (!descriptCheck.match(regex)) {
        descriptionContainer.classList.add("has-error");
    }
    else {
        descriptionContainer.classList.remove("has-error");
        descriptionContainer.classList.add("has-success");
    }
})

file.addEventListener("change", function(ev){
    ev.preventDefault();
    var regex = /\.(jpe?g|png|gif|bmp)$/;
    var fileCheck = file.value;
    var fileContainer = document.getElementById("fileContainer");
    var size = document.getElementById("image").files[0].size;
    
    if (!fileCheck.match(regex) || size > 104857600) {
        document.getElementById("eror-msg").innerHTML = "only jpeg/png/gif/bmp are supported";
    }
    else{
        fileContainer.classList.remove("has-error");
        fileContainer.classList.add("has-success");
        
    }
})








