var userName = document.getElementById("name");
var userEmail = document.getElementById("email");
var phone = document.getElementById("number");
var file= document.getElementById("image");


userName.addEventListener("keyup", function(ev){
        ev.preventDefault();
        var regex =  /^[a-zA-Z]+[\-]?\s?[a-zA-Z]*$/;
        var nameCheck = userName.value;
        var nameContainer = document.getElementById("name-container");
        if (!nameCheck.match(regex)) {
            nameContainer.classList.add("has-error");
            }
        else{
            nameContainer.classList.remove("has-error");
            nameContainer.classList.add("has-success");

        }

    })

userEmail.addEventListener("keyup", function(ev){
    ev.preventDefault();
    var regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var emailCheck = userEmail.value;
    var emailContainer = document.getElementById("email-container");
    
    if (!emailCheck.match(regex)) {
        emailContainer.classList.add("has-error");
    }
    else {
        emailContainer.classList.remove("has-error");
        emailContainer.classList.add("has-success");
    }
})

phone.addEventListener("keyup", function(ev){
    ev.preventDefault();
    var regex = /^0[2|3|7][0-9]{8,9}$/;
    var phoneCheck = phone.value;
    var phoneContainer = document.getElementById("phone-container");
    
    if (!phoneCheck.match(regex)) {
        phoneContainer.classList.add("has-error");
    }
    else {
        phoneContainer.classList.remove("has-error");
        phoneContainer.classList.add("has-success");
    }
})

file.addEventListener("change", function(ev){
    ev.preventDefault();
    var regex = /\.(jpe?g|png|gif|bmp)$/;
    var fileCheck = file.value;
    var fileContainer = document.getElementById("fileContainer");
    var size = document.getElementById("image").files[0].size;
    
    if (!fileCheck.match(regex) || size > 104857600) {
        document.getElementById("error-msg").innerHTML = "only jpeg/png/gif/bmp are supported";
    }
    else{
        fileContainer.classList.remove("has-error");
        fileContainer.classList.add("has-success");
        
    }
})
